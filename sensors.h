#include <Adafruit_BMP280.h>
#include <Adafruit_BNO055.h>
#include <Adafruit_Sensor.h>
#include <utility/imumaths.h>

#if !defined(SENSORS_H)
#define SENSORS_H

#define METERTOFEET 3.28084
#define GRAV 32.17405

// BN055 functions
// get linear acceleration
// get qutarnions

/* Connections
   ===========
   Connect SCL to analog 5
   Connect SDA to analog 4
   Connect VDD to 3.3-5V DC
   Connect GROUND to common ground
*/

// template <uint8_t N> imu::Vector<N> {
//     imu::Vector<N> trans_vec;
//     return trans_vec;
// }

class BMP280 {
  private:
    float rho = 1.225;
    float g = 9.807;
    Adafruit_BMP280 sensor;

  public:
    void attach_sensor(Adafruit_BMP280 bmp) {
        sensor = bmp;
        // BMP's "settings"
        sensor.setSampling(
            Adafruit_BMP280::MODE_NORMAL,  /* Operating Mode. */
            Adafruit_BMP280::SAMPLING_X16, /* Temp. oversampling */
            Adafruit_BMP280::SAMPLING_X16, /* Pressure oversampling */
            Adafruit_BMP280::FILTER_X16,   /* Filtering. */
            Adafruit_BMP280::STANDBY_MS_1);
    }

    double altitude() { return sensor.readAltitude(1013.25) * METERTOFEET; }

    double vertical_speed(double elapsed) {
        static double prev_alt =
            500; // intial value doesn't matter much, gets updated every loop
        double alt = altitude();
        double delta_alt = alt - prev_alt;
        prev_alt = alt;
        return delta_alt / elapsed;
    }
};

class BNO055 {
  private:
    Adafruit_BNO055 sensor;

  public:
    void attach_sensor(Adafruit_BNO055 bno) { sensor = bno; }

    imu::Vector<3>
    linear_acceleration() { // prints linear acceleration from accelerometr
        imu::Vector<3> accel =
            sensor.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL);
        return accel * METERTOFEET;
    }

    imu::Vector<3>
    acceleration() { // prints linear acceleration from accelerometr
        imu::Vector<3> accel =
            sensor.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
        return accel * METERTOFEET;
    }

    imu::Vector<3> gravity() { // prints linear acceleration from accelerometr
        imu::Vector<3> accel =
            sensor.getVector(Adafruit_BNO055::VECTOR_GRAVITY);
        return accel * METERTOFEET;
    }

    imu::Quaternion quaternions() { // prints qutornions from accelerometr
        return sensor.getQuat();
    }

    imu::Matrix<3> transformation_matrix() {
        imu::Quaternion quat = quaternions();
        double a = quat.w();
        double b = quat.x();
        double c = quat.y();
        double d = quat.z();

        imu::Vector<3> row1 =
            imu::Vector<3>(a * a + b * b - c * c - d * d, 2 * b * c - 2 * a * d,
                           2 * b * d + 2 * a * c);
        imu::Vector<3> row2 =
            imu::Vector<3>(2 * b * c + 2 * a * d, a * a - b * b + c * c - d * d,
                           2 * c * d - 2 * a * b);
        imu::Vector<3> row3 =
            imu::Vector<3>(2 * b * d - 2 * a * c, 2 * c * d + 2 * a * b,
                           a * a - b * b - c * c + d * d);

        imu::Matrix<3> trans_mat;
        trans_mat.vector_to_row(row1, 0);
        trans_mat.vector_to_row(row2, 1);
        trans_mat.vector_to_row(row3, 2);
        return trans_mat;
    }

    imu::Vector<3> inertial_acceleration() {
        imu::Matrix<3> trans_mat = transformation_matrix();
        imu::Vector<3> accel = linear_acceleration();
        imu::Vector<3> inertial_accel;
        for (int i = 0; i < 3; i++) {
            imu::Vector<3> row = trans_mat.row_to_vector(i);
            inertial_accel[i] = row.dot(accel);
        }
        return inertial_accel;
    }

    double vertical_acceleration() {
        imu::Vector<3> accel = inertial_acceleration();
        return accel.z();
    }
};

#endif // SENSORS_H
