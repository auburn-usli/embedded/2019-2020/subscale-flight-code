#include "logger.h"
#include "sensors.h"
#include "vehicle.h"
#include <Wire.h>

#define LAUNCH_TRIGGER 50
#define DRAG_TRIGGER 1000

enum Runmode {
    STANDBY = (1 << 0), // Sitting on the launch pad
    LAUNCH = (1 << 1),  // motor is burning, waiting for burn out
    DRAG = (1 << 2),    // drag routinne (for subscale just log data)
    DESCENT = (1 << 3), // stop drag and data logging and close stuff down
    GO = (1 << 4),      // flight is all good to go
    NOGO = (1 << 5),    // flight is a bust
    NO_ALT = (1 << 6),  // BMP280 failed to connect
    NO_IMU = (1 << 7),  // BNO055 failed to connect
    NO_SD = (1 << 8)    // SD card failed to connect
};

String headers[] = {"Time [s]", "Acceleration [ft/s^2]", "Velocity [ft/s]",
                    "Altitude [ft]", "Proj. Altitude [ft]"};

DataLogger datalog;
EventLogger eventlog;

BitFlag MODE; // abstract away bitwise operations on flag

Adafruit_BNO055 bno = Adafruit_BNO055(55, 0x28);
Adafruit_BMP280 bmp;

BMP280 altimeter;
BNO055 imu_sensor;

bool SWITCH_MODE = false;
double apogee = 0;
double ground_alt = 0;

void setup() {
    MODE.enable(STANDBY | GO);
    Serial.begin(115200);
    pinMode(LED_PIN, OUTPUT);

    if (!SD.begin(chipSelect)) {
        Serial.println("Initialization failed!");
        MODE.disable(GO);
        MODE.enable(NO_SD); // turn off GO and turn on NO_IMU
    }

    if (MODE == NO_SD) {
        while (1) { // hang and blink, so we know the SD card isn't in
            delay(50);
            blink_led();
        }
    }

    datalog.initialize("DATA", headers, 5);
    eventlog.initialize("LOG");

    // try connection to imu
    if (!bno.begin()) {
        Serial.println("BNO did not connect");
        MODE.enable(NO_IMU); // turn off GO and tu rn on NO_IMU
        eventlog.error("BNO055 failed to connect");
    } else {
        eventlog.event("BNO055 connected successfully");
        imu_sensor.attach_sensor(bno);
    }

    // try connection to altimeter
    if (!bmp.begin()) {
        Serial.println("Problem with altimeter.");
        MODE.enable(NO_ALT);
        eventlog.error("BMP280 failed to connect");
    } else {
        eventlog.event("BMP280 connected successfully");
        altimeter.attach_sensor(bmp);
        ground_alt = altimeter.altitude();
        eventlog.event("Ground altitude: " + String(ground_alt) + " ft");
    }

    if (MODE == (NO_IMU | NO_ALT)) {
        MODE.enable(NOGO);
        MODE.disable(GO);
        eventlog.error("BNO055 and BMP280 failed to connect - flight is NOGO");
    }

    if (MODE == GO) {
        // led on constantly means we're good
        digitalWrite(LED_PIN, HIGH);
    }

    // this block runs if flight is a no go
    else if (MODE == NOGO) {
        while (1) {
            delay(1000);
            blink_led();
        }
    }
}

void loop() {
    delay(50); // give the sensors some time to update their values

    if (MODE != NO_ALT &&
        MODE != NO_IMU) // all sensors are connected, so run like normal
    {
        double elapsed = elapsed_time();
        double alt = altimeter.altitude();
        double veloc = altimeter.vertical_speed(elapsed);
        double accel = imu_sensor.vertical_acceleration() - GRAV;

        if (MODE == STANDBY) {
            if (accel > (3 * GRAV)) {
                eventlog.event("BNO055 trigger");
                SWITCH_MODE = true;
            } else if (alt > (LAUNCH_TRIGGER + ground_alt)) {
                eventlog.event("BMP280 trigger");
                SWITCH_MODE = true;
            }
            if (SWITCH_MODE) {
                MODE.disable(STANDBY);
                MODE.enable(DRAG);
                eventlog.event("Switching to LAUNCH mode");
                SWITCH_MODE = false;
            }
        }

        else if (MODE == LAUNCH) {
            // wait for negative acceleration or altitude above some level
            // we'll just skip this for now
            if (accel < -GRAV || alt > (ground_alt + DRAG_TRIGGER))
                SWITCH_MODE = true;
            if (SWITCH_MODE) {
                MODE.disable(LAUNCH);
                MODE.enable(DRAG);
                eventlog.event("Switching to DRAG mode");
                SWITCH_MODE = false;
            }
        }

        else if (MODE == DRAG) {
            double proj_alt = projected_altitude(alt, veloc, accel);
            datalog[0] = (millis() / 1000.0);
            datalog[1] = accel;
            datalog[2] = veloc;
            datalog[3] = alt;
            datalog[4] = proj_alt;
            datalog.data_write();

            imu::Vector<3> acc = imu_sensor.inertial_acceleration();
            // Serial.println(String(acc.x()) + "," + String(acc.y()) + "," +
            //                String(acc.z()));
            Serial.println(veloc);

            apogee = (alt > apogee) ? alt : apogee;

            if (veloc < -25.0)
                SWITCH_MODE = true;

            if (SWITCH_MODE) {
                MODE.disable(DRAG);
                MODE.enable(DESCENT);
                eventlog.event("Switching to DESCENT mode");
                SWITCH_MODE = false;
            }
        }

        else if (MODE == DESCENT) {
            if (!SWITCH_MODE) {
                eventlog.event("Apogee of vehicle: " + String(apogee) + " ft");
                SWITCH_MODE = true; // dont want to keep logging the apogee
            }
        }
    }

    // Altimeter is connected, but not the IMU
    else if (MODE == NO_IMU || MODE != NO_ALT) {
        Serial.println("IMU is not connected");
    }

    // IMU is connected, but not the altimeter
    else if (MODE != NO_IMU || MODE == NO_ALT) {
        Serial.println("BMP is not connected");
    }
}
