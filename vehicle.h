/**
 * Contains "utility" functions that does extra things like blink the builtin
 * led, calculate the time between loops, etc. BitFlag is for creating an
 * integer whose bits can be turned on and off, so it is basically a
 * multi-boolean variable. Mutliple "flags" can be combined to make more complex
 * decisions.
 */

#ifndef VEHICLE_H
#define VEHICLE_H

#include <math.h>

#ifndef GRAV
#define GRAV 32.17405
#endif

#define LED_PIN 13

double elapsed_time() {
    static double prev_time = 0;
    double now = millis();
    double elapsed = now - prev_time;
    prev_time = now;
    return elapsed / 1000;
}

// blink led if there's a problem
void blink_led() {
    static bool pin_on = false;
    if (pin_on) {
        digitalWrite(LED_PIN, LOW);
        pin_on = false;
    } else {
        digitalWrite(LED_PIN, HIGH);
        pin_on = true;
    }
}

// An abstraction over bitwise operations on a BitFlag, so this is basically a
// flag You can combine flags to turn a bunch of things on and off at once. To
// combine them, write flag1 | flag2, so to enable two flags, write
// bitflag.enable(FLAG1 | FLAG2); etc...
class BitFlag {
    int m_flag;

  public:
    BitFlag() { m_flag = 0; }
    void enable(const int &flags) { m_flag |= flags; }
    void disable(const int &flags) { m_flag &= ~flags; }
    void toggle(const int &flags) { m_flag ^= flags; }
    bool operator==(const int &flag) { return (m_flag & flag) == flag; }
    bool operator!=(const int &flag) { return !((m_flag & flag) == flag); }
};

// For the pid controller later
double projected_altitude(double alt, double veloc, double accel) {
    return abs((veloc * veloc) / (2 * (accel + GRAV))) *
               log(abs(accel / GRAV)) +
           alt;
}

#endif // VEHICLE_H
