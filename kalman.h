#if !defined(KALMAN_H)
#define KALMAN_H

#include <utility/imumaths.h>

template <uint8_t N>
class Kalman
{
    imu::Matrix<N> state_transition;
    imu::Matrix<N> control;
    imu::Vector<N> observation;
    imu::Vector<N> state_estimate;
    imu::Vector<N> measurement_error;

    imu::Vector<N> operator()(imu::Vector<N> measurement, double dt)
    {
        static imu::Vector<N> p_measure = imu::Vector<N>();
        imu::Vector<N> prediction = state_transition * measurement;
    }
};

#endif // KALMAN_H
