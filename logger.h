/**
 * For data and event logging. Depending on how a Logger object is created,
 * it will write data to a text file in different ways. There are methods
 * for writing data in columns like an Excel table, and others for writing
 * events and error messages.
 */

#ifndef _DATALOG_h
#define _DATALOG_h
#include <SD.h>
#include <SPI.h>
#include <math.h>
#include <stdio.h>

#define NUMCOL 32
#define COLWIDTH 20

const String LOG_TITLE("A U B U R N    U N I V E R S I T Y    R O C K E T R Y  "
                       "  A S S O C I A T I O N");
const String SUB_TITLE("S U B S C A L E    F L I G H T    L O G");

String repeating_char(const char c, int repeats) {
    String str;
    for (int i = 0; i < repeats; i++) {
        str += c;
    }
    return str;
}

String center_string(String str, int textwidth) {
    int whitespace = textwidth - str.length();
    int pad = whitespace / 2;
    String center_str;
    for (int i = 0; i < pad; i++) {
        center_str += " ";
    }
    return center_str + str;
}

const int chipSelect = BUILTIN_SDCARD;

/**
 * Base Logger class
 */
class Logger {
  protected:
    String filename;

  public:
    // Create a indent to pad values in a column
    String create_indent(int num_spaces) {
        String pad_str = String("");
        for (int i = 0; i < num_spaces; ++i) {
            pad_str += String(" ");
        }
        return pad_str;
    }
    // Number of whole digits for each data point
    int data_len(double data) { return String(data).length(); }

    // Create a unique file name every time the microcontroller boots, so we
    // don't overwrite any data on accident. Prefix stands for the beginning of
    // the file name, like DATA or LOGS
    String create_file_name(const char *prefix) {
        int num_files = 0;
        File root = SD.open("/"); // start at root and walk all files
        while (true) {
            File entry = root.openNextFile();
            if (!entry) // no more files
                break;
            else if (entry.isDirectory()) // skip over directories
                continue;
            else {
                String current_file = entry.name();
                if (current_file.startsWith(prefix))
                    num_files++;
            }
        }
        String new_filename = String(prefix) + "_" + num_files + ".txt";
        return new_filename;
    }
};

/**
 * DataLogger class derived from Logger class
 */

class DataLogger : public Logger {
    String headers[NUMCOL];
    double data_array[NUMCOL];
    int num_hdr;

    void print_banner(int textwidth) {
        File file = SD.open(filename.c_str(), FILE_WRITE);
        if (file) {
            String border = center_string(
                repeating_char('=', LOG_TITLE.length()), textwidth);
            String title = center_string(LOG_TITLE, textwidth);
            String subtitle = center_string(SUB_TITLE, textwidth);
            file.println('\n' + border + "\n\n" + title + "\n\n" + subtitle +
                         "\n\n" + border + "\n\n");
        }
        file.close();
    }

  public:
    void initialize(const char *name, String header_array[], int num_headers) {
        num_hdr = num_headers;
        filename = create_file_name(name);
        int textwidth = COLWIDTH * num_hdr;
        print_banner(textwidth);
        File file = SD.open(filename.c_str(), FILE_WRITE);
        if (file) {
            String line;
            for (int i = 0; i < num_hdr; i++) {
                headers[i] = header_array[i];
                String pad_str = create_indent(COLWIDTH - headers[i].length());
                if (i == 0)
                    line += String(headers[i]) + pad_str;
                else
                    line += pad_str + String(headers[i]);
            }
            file.println(line);
        }
        file.close();
    }
    // pretty print text to file
    void data_write() {
        File file = SD.open(filename.c_str(), FILE_WRITE);
        if (file) {
            String line;
            for (int i = 0; i < num_hdr; i++) {
                String data = String(data_array[i]);
                int indent = COLWIDTH - data.length();
                String pad_str = create_indent(indent);
                if (i == 0)
                    line += data + pad_str;
                else
                    line += pad_str + data;
            }
            file.println(line);
        }
        file.close();
    }
    // Add data to array that Logger writes to file
    double &operator[](int index) { return data_array[index]; }
};

/**
 * EventLogger class derived from Logger
 */

class EventLogger : public Logger {
  private:
    void print_banner() {
        int textwidth = COLWIDTH * 5;
        File file = SD.open(filename.c_str(), FILE_WRITE);
        if (file) {
            String border = center_string(
                repeating_char('=', LOG_TITLE.length()), textwidth);
            String title = center_string(LOG_TITLE, textwidth);
            String subtitle = center_string(SUB_TITLE, textwidth);
            file.println('\n' + border + "\n\n" + title + "\n\n" + subtitle +
                         "\n\n" + border + "\n\n");
        }
        file.close();
    }

  public:
    void initialize(const char *name) {
        filename = create_file_name(name);
        print_banner();
    }
    // For event logs, creates a formatted time stamp string
    String time_stamp() {
        double seconds = millis() / 1000;
        double minutes = seconds / 60;
        double hours = minutes / 60;
        int whole_secs = (int)seconds;
        int whole_mins = (int)minutes;
        int whole_hours = (int)hours;
        String hours_str = (whole_hours < 10) ? "0" + String(whole_hours)
                                              : String(whole_hours);
        String mins_str =
            (whole_mins < 10) ? "0" + String(whole_mins) : String(whole_mins);
        String secs_str =
            (whole_secs < 10) ? "0" + String(whole_secs) : String(whole_secs);
        return hours_str + ":" + mins_str + ":" + secs_str;
    }
    // Simple writeln
    void writeln(String line) {
        File file = SD.open(filename.c_str(), FILE_WRITE);
        if (file)
            file.println(line);
        file.close();
    }

    void error(String msg) {
        String time = time_stamp();
        String line = "[ " + time + " ][ ERROR ]    " + msg;
        writeln(line);
    }

    void event(String msg) {
        String time = time_stamp();
        String line = "[ " + time + " ][ EVENT ]    " + msg;
        writeln(line);
    }
};

#endif
